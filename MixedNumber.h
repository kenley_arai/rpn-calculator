#ifndef MIXEDNUMBER_H
#define MIXEDNUMBER_H

#include "include.h"
#include "Fraction.h"

//should do += -= *= and \= as we'll be using those methods - no need for + and such as we want to create as few objects as possible
//how the two classes interact is up to you - if you want to do operarate.do(mixed1, mixed2) or whatever you can
//just decide and write the headers so I know what to do in the calculator

class MixedNumber : public Fraction
{
public:
    MixedNumber();
	MixedNumber(int Numerator, int Denominator = 1, int Whole = 0);

    string show() const;

    friend
    ostream& operator<<(ostream& out, const MixedNumber &other);  //output the mixed number
private:
	//Fraction imaginary;
};


MixedNumber* getMN(char* &cPtr);  //moved external as I don't want to have a MN object i'm calling this in (yes I changed my mind on this)
int getDigit(char* cPtr);
void getNumber(char* &cPtr, int &number, int &temp);

#endif // MIXEDNUMBER_H
