#ifndef INCLUDE_H
#define INCLUDE_H

#include <iostream>
#include <math.h>
#include <cstring>
#include <string>
#include <typeinfo>
#include <stdlib.h>

enum ERRORS { DATATYPE_MISMATCH, EMPTY, BAD_SIZE, FULL, BAD_INPUT, TOO_MANY_OPERATORS, TOO_MANY_NUMBERS,
			  LEFT_PAR_WITHOUT_RIGHT, RIGHT_PAR_WITHOUT_LEFT, UNKNOWN_ERROR, DIVISION_BY_ZERO,
			  BAD_NUMBER, UNDEFINED_OUTPUT, INTEGER_OVERFLOW, NO_INPUT};
//UNKNOWN_ERROR indicates a programming error... helpfull in debugging, but should never happen "live"

inline
std::string decodeERRORS(ERRORS e)
{
	std::string returnString("ERROR: ");
	switch (e)
	{
	case DATATYPE_MISMATCH:
		return returnString += "DataType Missmatch";
	case EMPTY:
		return returnString += "Empty";
	case BAD_SIZE:
		return returnString += "Bad Size";
	case FULL:
		return returnString += "Full";
	case BAD_INPUT:
		return returnString += "Unknown Character";
	case TOO_MANY_OPERATORS:
		return returnString += "Too many Operators";
	case TOO_MANY_NUMBERS:
		return returnString += "Too many Numbers";
	case LEFT_PAR_WITHOUT_RIGHT:
		return returnString += "'(' Without ')'";
	case RIGHT_PAR_WITHOUT_LEFT:
		return returnString += "')' Without '('";
	case DIVISION_BY_ZERO:
		return returnString += "Division By Zero";
	case BAD_NUMBER:
		return returnString += "Bad Number";
	case UNDEFINED_OUTPUT:
		return returnString += "Undefined Ouput on Power";
	case INTEGER_OVERFLOW:
		return returnString += "Integer Overflow";
	case NO_INPUT:
		return returnString += "No Input";
	default:
		return returnString += "Unknown Error";
	}
}

inline
void skipSpace(char* &cPtr, bool reverse = false)
{
	while(*cPtr == ' ' || *cPtr == '\t' || *cPtr == '_' )
	{
		if (reverse)
			--cPtr;
		else
			++cPtr;
	}
}

#endif // INCLUDE_H
