#include "mixedNumber.h"


MixedNumber::MixedNumber()
{}

MixedNumber::MixedNumber(int Numerator, int Denominator, int Whole)
{
	if (Whole)
		safeAddEq(Numerator, safeMult(Whole, denominator));
	set(Numerator, Denominator);
}

string MixedNumber::show() const
{
	string returnString;
    stringstream ss;
	int whole = numerator / denominator;
	int numer = numerator % denominator;

	if(whole || !numer)
    {
        ss << whole;
		returnString += ss.str();
		ss.str(string());
        ss.clear();
    }

    // Doesn't show if the numerator is 0
	if(numer)
    {
        // This puts the numerator of the fraction
		if (whole)
		{
			returnString += "_";
			ss << abs(numer);
		}
		else
			ss << numer;
		returnString += ss.str();
		ss.str(string());
        ss.clear();

        // This puts the denominator of the fraction
        if(denominator != 1)
        {
			returnString += '/';
			ss << denominator;
			returnString += ss.str();
        }
    }
    return returnString;
}

std::ostream& operator<<(std::ostream& out, const MixedNumber &other)
{
    out << other.show();
    return out;
}


MixedNumber* getMN(char* &cPtr)
{

	int temp, numer = 0, denom = 1, whole = 0;
	if((*cPtr >= '0' && *cPtr <= '9') || *cPtr == '.')  //if we point to a number or a decimal place
	{
		getNumber(cPtr,numer,temp);  //get the first number
		if (*cPtr == '.')  //check for decimal place
		{
			whole = numer;  //if one found make the first number the whole
			numer = 0;
			++cPtr;
			while((temp = getDigit(cPtr)) >= 0)  //and get the decimals while making it into a fraction
			{
				safeMultEq(numer,10);
				safeAddEq(numer,temp);
				safeMultEq(denom,10);
				++cPtr;
			}
		}
		else if (*cPtr == '/')  //else is this a fraction?
		{
			++cPtr;
			denom = 0;
			getNumber(cPtr,denom,temp);  //yes get the denominator
		}
		else if(*cPtr == ' ' && (temp = getDigit(++cPtr)) >= 0 )  //else is this a mixed number?
		{
			whole = numer;  //yes make the first number a whole
			numer = temp;
			denom = 0;
			++cPtr;
			getNumber(cPtr,numer,temp);  //and get the numerator for the fraction
			if (*cPtr == '/')
			{
				++cPtr;
				getNumber(cPtr,denom,temp);  //then the denominator
			}
		}
		if (!denom)
			throw BAD_NUMBER;  //if there was no denominator found there is an error with the input
		skipSpace(cPtr);
		return new MixedNumber(numer, denom, whole);
	}
	return NULL;
}

inline
int getDigit(char* cPtr)
{
	if(*cPtr >= '0' && *cPtr <= '9')
		return *cPtr - '0';
	else
		return -1;
}

inline
void getNumber(char* &cPtr, int &number, int &temp)
{
	while((temp = getDigit(cPtr)) >= 0)
	{
		safeMultEq(number,10);
		safeAddEq(number,temp);
		++cPtr;
	}
}

