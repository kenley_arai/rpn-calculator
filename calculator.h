#ifndef CALCULATOR_H
#define CALCULATOR_H

#include "include.h"
#include "myQueue.h"
#include "myStack.h"
#include "Operator.h"
#include "MixedNumber.h"


class Calculator
{
	public:
		Calculator();
		~Calculator();

		void inputCString(char* cPtr);
		void inputMN(MixedNumber*  input);
		void inputOP(Operator*  input);
		void finishRPN();
		string& calculate();
		void clear();

        friend std::istream& operator>>(std::istream& in, Calculator &other);      //gets the line and converts it to RPN.
        friend std::ostream& operator<<(std::ostream& out, const Calculator &other);  //outputs the answer (the top of opStack)

		MixedNumber* last;
	private:
		myQueue rpnQueue;
		myStack calcStack;
		myStack opStack;
		string showRPN;

};


//helpers
template <typename T>
bool sameType(T &setThis, myQueue& Q);
template <typename T>
bool sameType(T &setThis, myStack& S);

void moveOP(myQueue& dest, myStack& src, Operator* &holder);
void moveOP(myStack& dest, myQueue& src, Operator* &holder);

void moveMN(myQueue& dest, myStack& src, MixedNumber* &holder);
void moveMN(myStack& dest, myQueue& src, MixedNumber* &holder);


#endif // CALCULATOR_H

