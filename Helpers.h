#ifndef HELPERS_H
#define HELPERS_H

//// helper functions used in templated linkedlist, Stack and Queue for char* compatabilty/////


template<typename A>
inline
void initIt(A)
{}

inline
void initIt(char* &p)
{
	p = NULL;
}

template<typename A>
inline
void initIt(A, int)
{}

inline
void initIt(char** &p, int size)
{
	for (int i = 0; i < size; i++)
		p[i] = NULL;
}

template<typename A>
inline
void delDy(A)
{}

inline
void delDy(char* &p)
{
	if (p)
	{
		delete[] p;
		p = NULL;
	}
}

template<typename A>
inline
void clearAr(A, int)
{}

inline
void clearAr(char** &a, int top)
{
	if (top < 0)
		return;
	for (int i = 0; i <= top; ++i)
	{
		if (a[i])
		{
			delete[] a[i];
			a[i] = NULL;
		}
	}
}

template<typename A>
inline
void copyIt(A &l, A const &r)
{
	l = r;
}

inline
void copyIt(char* &l, char* const &r)
{
	if (!r)
	{
		l = NULL;
		return;
	}
	if (l)
		delete[] l;
	l = new char[strlen(r) + 1];
	strcpy(l, r);
}

template<typename A>
inline
A& readnClear(A &readThis)
{
	return readThis;
}

inline
char* readnClear(char* &readThis)
{
	char* temp = readThis;
	readThis = NULL;
	return temp;
}

inline
std::istream& operator>>(std::istream& in, char* &p)
{
	std::string junk;
	in >> junk;
	if (p)
		delete[] p;
	p = new char[junk.length() + 1];
	strcpy(p, junk.c_str());
	return in;
}

template<typename A>
inline
bool isEq(const A l, const A r)
{
	return (l == r);
}
inline
bool isEq(char* const l, char* const r)
{
	return !strcmp(l, r);
}

template<typename A>
inline
bool isGr(const A l, const  A r)
{
	return (l > r);
}
inline
bool isGr(char* const l, char* const r)
{
	return (strcmp(l, r) > 0);
}

template<typename A>
inline
void swapIt(A &l, A &r)
{
	A hold = l;
	l = r;
	r = hold;
}

#endif // HELPERS_H
