#include <QtWidgets>
#include <QKeyEvent>
#include "button.h"
#include "GUI.h"

GUI::GUI(QWidget *parent)
    : QWidget(parent)
{

    //This is set to clear the input.
    waitingForInput = true;

    // Display object for the answer and creation of the RPN
    display = new QLineEdit("0");
    display->setReadOnly(true);
    display->setAlignment(Qt::AlignRight);
	display->setMaxLength(255);  //***was too short
    // The actual RPN
    /*displayRPN = new QLineEdit("0");
    displayRPN->setReadOnly(true);
    displayRPN->setAlignment(Qt::AlignRight);
    displayRPN->setMaxLength(25);

    The GUI does not like this second display
    */

    //Font for the display
    QFont font = display->font();
    font.setPointSize(font.pointSize());
    display->setFont(font);
    //displayRPN->setFont(font);

    // Numbper pad creation
    for (int i = 0; i < NumDigitButtons; ++i) {
        digitButtons[i] = createButton(QString::number(i), SLOT(digitClicked()));
    }

    // Operators button creation
    Button *divisionButton = createButton(tr("*"), SLOT(multiplicativeOperatorClicked()));
    Button *timesButton = createButton(tr("/"), SLOT(divisionOperatorClicked()));
    Button *minusButton = createButton(tr("-"), SLOT(subtractOperatorClicked()));
    Button *plusButton = createButton(tr("+"), SLOT(additiveOperatorClicked()));
    Button *equalButton = createButton(tr("="), SLOT(equalClicked()));
    Button *powerButton = createButton(tr("^"), SLOT(powerOperatorClicked()));
    Button *openParen = createButton(tr("("), SLOT(openParenOperatorClicked()));
    Button *closeParen = createButton(tr(")"), SLOT(closeParenOperatorClicked()));

    Button *delButton = createButton(tr("DEL"), SLOT(deleteClicked()));
    Button *decimalButton = createButton(tr("."), SLOT(decimalClicked()));
    Button *allClear = createButton(tr("AC"), SLOT(allClearClicked()));
    Button *spaceButton = createButton(tr("Space"), SLOT(spaceClicked()));

    //Grid object
    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->setSizeConstraint(QLayout::SetFixedSize);
    mainLayout->addWidget(display, 0, 0, 2, 9); // Creating the display box for the answer
   // mainLayout->addWidget(displayRPN, 0, 6, 8, 13); // Creating the display box for the answer

    for (int i = 1; i < NumDigitButtons; ++i) {
        int row = ((9 - i) / 3) + 2;
        int column = ((i - 1) % 3) + 1;
        mainLayout->addWidget(digitButtons[i], row, column);
    }

    //Assigning each widget it's own function
    mainLayout->addWidget(divisionButton, 3, 5);
    mainLayout->addWidget(timesButton, 3, 4);
    mainLayout->addWidget(minusButton, 4, 5);
    mainLayout->addWidget(plusButton, 4, 4);
    mainLayout->addWidget(powerButton, 5, 5);
    mainLayout->addWidget(openParen, 2, 4);
    mainLayout->addWidget(closeParen, 2, 5);
    mainLayout->addWidget(equalButton, 4, 6, 2 ,1);
    mainLayout->addWidget(decimalButton, 5, 4);
    mainLayout->addWidget(delButton, 2, 6, 1, 1);
    mainLayout->addWidget(allClear, 3, 6, 1, 1);
    mainLayout->addWidget(spaceButton, 5, 2, 1, 2);
    mainLayout->addWidget(digitButtons[0], 5, 1);
    setLayout(mainLayout);

    setWindowTitle(tr("Calculator"));
}

void GUI::keyPressEvent(QKeyEvent *e)
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }

    if(e->key() == Qt::Key_Return)
        equalClicked();
    else if(e->key() == Qt::Key_Delete)
        deleteClicked();
    else if(e->key() == Qt::Key_Backspace)
        deleteClicked();
    else
        display->setText(display->text().append(e->key()));
}

void GUI::digitClicked()
{
    Button *clickedButton = qobject_cast<Button *>(sender());

    int digitValue = clickedButton->text().toInt();

    if (display->text() == "0" && digitValue == 0.0)
        return;

    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }

    display->setText(display->text() + QString::number(digitValue));
}

void GUI::spaceClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }

    display->setText(display->text().append(" "));
}

void GUI::additiveOperatorClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }

    display->setText(display->text().append("+"));
}

void GUI::subtractOperatorClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }
    display->setText(display->text().append("-"));
}

void GUI::divisionOperatorClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }
    display->setText(display->text().append("/"));
}

void GUI::multiplicativeOperatorClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }
    display->setText(display->text().append("*"));
}

void GUI::powerOperatorClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }
    display->setText(display->text().append("^"));
}

void GUI::openParenOperatorClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }
    display->setText(display->text().append("("));
}

void GUI::closeParenOperatorClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }
    display->setText(display->text().append(")"));
}

void GUI::decimalClicked()
{
    if(waitingForInput)
    {
        display->clear();
        waitingForInput = false;
    }
    display->setText(display->text().append("."));
}

void GUI::allClearClicked()
{
    display->clear();
    waitingForInput = true;
    display->setText("0");
}

void GUI::deleteClicked()
{
    if(display->text() != "0")
    {
        QString tmp = display->text();
        tmp.chop(1);
        display->setText(tmp);
//        display->text().chop(1);
//        display->setText(display->text());
        // It doesn't like directly chopping the qString
    }
    else
    {
      display->clear();
      display->setText("0");
    }
}

void GUI::equalClicked()
try
{
	if(waitingForInput)
	{
		display->clear();
		waitingForInput = false;
	}
	char line[257];
	*line = '\0'; //initialize the start of line;
	strcpy(line+1, display->text().toStdString().c_str());

	RPN.inputCString(line+1);
    display->setText(QString().fromStdString(RPN.calculate()));
    waitingForInput = true;
}
catch(ERRORS e)
{
	waitingForInput = true;
	RPN.clear();
    display->clear();
	display->setText(decodeERRORS(e).c_str());
}

Button *GUI::createButton(const QString &text, const char *member)
{
    Button *button = new Button(text);
    connect(button, SIGNAL(clicked()), this, member);
    return button;
}
