#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "node.h"

template<typename T = char>
class linkedList  //a generic non-sorted, non-unique linkedList ripe for inheriting from
{
public:
	linkedList();
	linkedList(const T& data);
	linkedList(const linkedList<T> &other);
	~linkedList();

	linkedList<T>& operator=(const linkedList<T> &other);  //sets a linkedList to the same as another
	linkedList<T>& operator+=(const linkedList<T> &other);   //appends another linkedList to the end of this one

	void insert(const T &insertThis);  //inserts at the head
	void remove(const T &removeThis);
	T removeHead();
	linkedList<T>& operator<<(const T &data);
	linkedList<T>& operator>>(T &data);
	void clear();

	bool empty()const;
	bool find(const T& data);

	template<typename U>
	friend std::ostream& operator<<(std::ostream &out, const linkedList<U> &list);

	template<typename U>
	friend std::istream& operator>>(std::istream &in, linkedList<U> &list);


protected:
	node<T> anchor;
	void remove(node<T>* removeThis);
	void removeAfter(node<T>* afterThis);
	void insertAfter(node<T>* afterThis, const T &insertThis);
	node<T>* nodeBefore(node<T>* beforeThis);
	node<T>* nodeBefore(const T &data);               //used in remove()

private:
	void copy(const linkedList<T> &other);
	void nukem();
};


template<typename T>
linkedList<T>::linkedList()
{}

template<typename T>
linkedList<T>::linkedList(const T &data)
{
	anchor.nextNode() = new node<T>(data);
}

template<typename T>
linkedList<T>::linkedList(const linkedList<T> &other)
{
	copy(other);
}

template<typename T >
linkedList<T>::~linkedList()
{
	nukem();
}

template<typename T>
linkedList<T>& linkedList<T>::operator=(const linkedList<T> &other)
{
	if (this != &other)
	{
		nukem();
		copy(other);
	}
	return *this;
}

template<typename T>
linkedList<T>& linkedList<T>::operator+=(const linkedList<T> &other)   //this one wasn't defined
{
	copy(other);
	return *this;
}

template<typename T>
void linkedList<T>::insert(const T &data)
{
	node<T>* newNode = new node<T>(data);
	newNode->nextNode() = anchor.nextNode();
	anchor.nextNode() = newNode;
}

template<typename T>
void linkedList<T>::remove(const T& data)
{
	if (!anchor.nextNode())
		throw EMPTY;
	node<T> *who = nodeBefore(data);
	if (who->nextNode())
	{
		node<T>*bye = who->nextNode();
		who->nextNode() = bye->nextNode();
		delete bye;
	}
	else
		throw BAD_INPUT;
}

template<typename T>
T linkedList<T>::removeHead()
{
	T hold = readnClear(anchor.nextNode()->theData());
	removeAfter(&anchor);
	return hold;
}

template<typename T>
linkedList<T>& linkedList<T>::operator>>(T &data)
{
	remove(data);  //why rewrite the wheel? (just more likely to make errors)
	return *this;
}

template<typename T>
linkedList<T>& linkedList<T>::operator<<(const T &data)
{
	insert(data);
	return *this;
}

template<typename T>
void linkedList<T>::clear()
{
	nukem();
}

template<typename T>
bool linkedList<T>::empty() const
{
	return !anchor.nextNode();
}

template<typename T>
bool linkedList<T>::find(const T &data)
{
	return nodeBefore(data)->nextNode();
}

template<typename T>
void linkedList<T>::remove(node<T>* removeThis)
{
	removeAfter(nodeBefore(removeThis));
}

template<typename T>
void linkedList<T>::removeAfter(node<T>* afterThis)
{
	node<T>* goner = afterThis->nextNode();
	afterThis->nextNode() = goner->nextNode();
	delete goner;
}

template<typename T>
void linkedList<T>::insertAfter(node<T>* afterThis, const T &insertThis)
{
	node<T>* newNode = new node<T>(insertThis);
	newNode->nextNode() = afterThis->nextNode();
	afterThis->nextNode() = newNode;
}

template<typename T>
node<T>* linkedList<T>::nodeBefore(node<T>* beforeThis)
{
	node<T>* who = &anchor;
	for (; who->nextNode() == beforeThis; who = who->nextNode());
	return who;
}

template<typename T>
node<T>* linkedList<T>::nodeBefore(const T &data)
{
	node<T>* who = &anchor;
	for (; who->nextNode() && !isEq(who->nextNode()->theData(), data);  //fixed (missing ->.nextNode())
		who = who->nextNode());
		return who;
}

template<typename T>
void linkedList<T>::copy(const linkedList<T> &other)
{
	for (node<T> *who = other.anchor.nextNode(); who; who = who->nextNode())    //fixed (missing .achor)
		insert(*who);
}

template<typename T>
void linkedList<T>::nukem()
{
	node<T> *bye = anchor.nextNode();
	while (bye)
	{
		anchor.nextNode() = anchor.nextNode()->nextNode();
		delete bye;
		bye = anchor.nextNode();
	}
}

template<typename U>
std::ostream& operator<<(std::ostream &out, const linkedList<U> &list)
{
	for (const node<U> *who = list.anchor.nextNode(); who; who = who->nextNode())
		out << *who;
	return out;
}

template<typename U>
std::istream& operator>>(std::istream &in, linkedList<U> &list)
{
	U data;
	initialize(data);
	if (in >> data)
		list.insert(data);
	return in;
}

#endif  // LINKEDLIST_H
