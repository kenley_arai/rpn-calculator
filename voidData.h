#ifndef VOIDDATA_H
#define VOIDDATA_H

#include <typeinfo>

struct voidData
{
	template <typename T>
	voidData(const T Data)
	{
		T* hold = new T;
		*hold = Data;
		data = hold;
		type = &typeid(Data);
	}
	void* data;
	const std::type_info* type;
};

#endif // VOIDDATA_H
