#ifndef MYQUEUE_LL_H
#define MYQUEUE_LL_H

#include <sstream>
#include "linkedList.h"

template<typename T = char>
class Queue :
		private linkedList<T>
{
  public:
	Queue(int s = 5);
	~Queue();
	Queue(const Queue<T> &other);
	Queue<T>& operator=(const Queue<T> &other);

	void enqueue(const T &d);
	T dequeue();
	Queue<T>& operator<<(const T &d);  // Enqueueing
	Queue<T>& operator>>(T &d);		// Dequeueing
	void swap(Queue<T> &other);
	void clear();
	void resize(int s = 10);

	bool full()const;
	bool empty()const;
	int size()const;
	const T& front() const;
	const T& back() const;

	template<typename U>
	friend std::ostream& operator<<(std::ostream& out, const Queue<U> &theQueue);
	template<typename U>
	friend std::istream& operator>>(std::istream& in, Queue<U> &theQueue);

  private:
	node<T>* tail;
	int count, capacity;

	void copy(const Queue<T> &other);
	void nukem();

};

template<typename T>
Queue<T>::Queue(int s)
{
	if(s < 1)
		throw BAD_SIZE;
	capacity = s;
	count = 0;
}

template<typename T>
Queue<T>::~Queue()
{
	nukem();
}

template<typename T>
Queue<T>::Queue(const Queue<T> &other)
{
	copy(other);
}

template<typename T>
Queue<T>& Queue<T>::operator=(const Queue<T> &other)
{
	if(this != &other)
	{
		nukem();
		copy(other);
	}
	return *this;
}

template<typename T>
void Queue<T>::enqueue(const T& d)
{
	if(full())
		throw FULL;

	if(empty())
	{
		tail = new node<T>(d);
		linkedList<T>::anchor.nextNode() = tail;
	}
	else
	{
		node<T>* builder = new node<T>(d);
		tail->nextNode() = builder;
		tail = builder;
	}
	++count;
}

template<typename T>
T Queue<T>::dequeue()
{
	if(empty())
		throw EMPTY;
	--count;
	return linkedList<T>::removeHead();
}

template<typename T>
Queue<T>& Queue<T>::operator<<(const T &d)  // Enqueueing
{
	enqueue(d);
	return *this;
}

template<typename T>
Queue<T>& Queue<T>::operator>>(T &d)// Dequeueing
{
	d = dequeue();
	return *this;
}

template<typename T>
void Queue<T>::clear()
{
	nukem();
}

template<typename T>
bool Queue<T>::full() const
{
	return (count == capacity);
}

template<typename T>
bool Queue<T>::empty() const
{
	return !count;
}

template<typename T>
int Queue<T>::size() const
{
	return count;
}

template<typename T>
void Queue<T>::resize(int s)
{
	if(s < 1 || s < count)
		throw BAD_SIZE;
	capacity = s;
}

template<typename T>
void Queue<T>::swap(Queue<T> &other)
{
	swapIt(linkedList<T>::anchor.nextNode(), other.anchor.nextNode());
	swapIt(count, other.count);
	swapIt(capacity, other.capacity);
	swapIt(tail, other.tail);
}

template<typename T>
const T& Queue<T>::front() const
{
	if (empty())
		throw EMPTY;
	return linkedList<T>::anchor.nextNode()->theData();
}

template<typename T>
const T& Queue<T>::back() const
{
	if (empty())
		throw EMPTY;
	return tail->theData();
}

template<typename T>
void Queue<T>::copy(const Queue<T> &other)
{
	capacity = other.capacity;
	node<T>* afterThis = &this->anchor;
	for (const node<T>* walker = other.anchor.nextNode(); walker; walker = walker->nextNode())
	{
		insertAfter(afterThis, walker->theData());
		afterThis = afterThis->nextNode();
		++count;
	}
}

template<typename T>
void Queue<T>::nukem()
{
	count = 0;
	linkedList<T>::clear();
}


template<typename U>
std::ostream& operator<<(std::ostream& out, const Queue<U> &other)
{
	if(out == std::cout)
	{
		if(other.empty())
		{
			out << "The Queue is empty." << std::endl;
			return out;
		}
		else
			out << "Queue contents:" << std::endl;
	}
	else
		out<<"Queue Capacity : "<<other.capacity<<std::endl;
	for (const node<U>* walker = other.anchor.nextNode();
					walker; walker = walker->nextNode())
		out << *walker << std::endl;
	return out;
}



template<typename U>
std::istream& operator>>(std::istream& in, Queue<U> &other)
{
	other.nukem();
	U data;
	initIt(data);
	if(in == std::cin)
	{
		std::cout << "Press Enter between inputs then input Ctrl-Z to finish" << std::endl;
		while (in >> data)
			other.enqueue(data);
		if (!in.eof())
			throw BAD_INPUT;
		in.clear();
	}
	else
	{
		std::string theLine;
		getline(in, theLine);
		theLine = theLine.substr(theLine.find(':')+2);
		other.capacity = atoi(theLine.c_str());

		while (in >> data)
			other.enqueue(data);
		if (in.fail() && !in.eof())
			throw BAD_INPUT;
	}
	delDy(data);
	return in;
}

#endif // MYQUEUE_LL_H
