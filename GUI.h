#ifndef GUI_H
#define GUI_H

#include <QtWidgets>
#include "calculator.h"

class QLineEdit;

class Button;

class GUI : public QWidget
{
    Q_OBJECT

public:
    GUI(QWidget *parent = 0);

protected:
    virtual void keyPressEvent(QKeyEvent *e);

private slots:
    void digitClicked();
    void additiveOperatorClicked();
    void subtractOperatorClicked();
    void divisionOperatorClicked();
    void multiplicativeOperatorClicked();
    void equalClicked();
    void spaceClicked();
    void powerOperatorClicked();
    void openParenOperatorClicked();
    void closeParenOperatorClicked();
    void decimalClicked();
    void allClearClicked();
    void deleteClicked();

private:
    Button *createButton(const QString &text, const char *member);
    Calculator RPN;
    QLineEdit *display;
    //QLineEdit *displayRPN;
    bool waitingForInput;
    enum { NumDigitButtons = 10 };
    Button *digitButtons[NumDigitButtons];
};

#endif
