#include "Fraction.h"
#include <climits>	  // INT_MAX

Fraction::Fraction()
{
	denominator = 1;
	numerator = 0;
}

Fraction::Fraction(int Numerator, int Denominator)
{
	set(Numerator, Denominator);
}

Fraction::Fraction(const Fraction &other)
{
	set(other.numerator, other.denominator);
}

Fraction &Fraction::operator=(const Fraction &other)
{
	set(other.numerator, other.denominator);
    return *this;
}

Fraction::~Fraction()
{
    numerator = denominator = 0;
}

void Fraction::set(int num, int den)
{
    if(den == 0)
        throw DIVISION_BY_ZERO;

    numerator = num;
    denominator = den;
	simplify(numerator, denominator);
}

int Fraction::gcd(int num,int den)
{
    if(den == 0)
        return num;
	return gcd( den, num % den);  //recursive function until den == 0
}

void Fraction::simplify(int num, int den)
{
	int GCD = gcd(abs(num),den);  //we want to keep the same signs, we don't want GCD to be negative
	numerator = num/GCD;  //divide out the gcd
	denominator = den/GCD;
}

string Fraction::show() const
{
    string returnString = "";
    stringstream ss;

    // Only returns a fraction if numerator != 0 else it returns 0
    if(numerator != 0)
    {
        //This puts the numerator into the string
        ss << numerator;
        returnString += ss.str();
        ss.clear();
        ss.str(string());

        //This puts the denominator in the string
        if(denominator != 1)
        {
            ss << denominator;
            returnString += "/";
            returnString += ss.str();
        }
    }
    else
        returnString = "0";

    return returnString;
}

Fraction &Fraction::operator +=(const Fraction &other)
{
	if(denominator != other.denominator)
	{
		int otherNum = safeMult(other.numerator,denominator);

		safeMultEq(numerator, other.denominator);
		safeMultEq(denominator, other.denominator);

		safeAddEq(numerator, otherNum);
	}
	else
		safeAddEq(numerator,other.numerator);

	simplify(numerator, denominator);
    return *this;
}

Fraction &Fraction::operator -=(const Fraction &other)
{
	if(denominator != other.denominator)
	{
		int otherNum = safeMult(other.numerator,denominator);

		safeMultEq(numerator, other.denominator);
		safeMultEq(denominator, other.denominator);

		safeAddEq(numerator, -otherNum);
	}
	else
		safeAddEq(numerator, -other.numerator);


	simplify(numerator, denominator);
    return *this;
}

Fraction &Fraction::operator *=(const Fraction &other)
{
	safeMultEq(numerator,other.numerator);
	safeMultEq(denominator,other.denominator);
	simplify(numerator,denominator);
    return *this;
}

Fraction &Fraction::operator /=(const Fraction &other)
{
	if (!other.numerator)
		throw DIVISION_BY_ZERO;
	safeMultEq(numerator,other.denominator);
	safeMultEq(denominator,other.numerator);
	simplify(numerator, denominator);
    return *this;
}

Fraction &Fraction::operator ^=(const Fraction &other)
{
	double answ;
    double power = (other.numerator*1.0)/other.denominator;
    if (power < 0)
    {
        set(denominator, numerator);
        power *= -1;
    }
    if (other.denominator != 1)
	{
		if (numerator < 0)
			throw UNDEFINED_OUTPUT;
		int whole;
        answ = pow((numerator*1.0)/denominator, power);
		if (answ > INT_MAX)
			throw INTEGER_OVERFLOW;
		whole = (int)answ;
		answ -= whole;
		numerator = (int)(answ * 10000 + .05);
		denominator = 10000;
		simplify(numerator, denominator);
		safeAddEq(numerator,safeMult(whole,denominator));

	}
	else
	{
        answ = pow(numerator*1.0, power) + .05;
		if (answ > INT_MAX)
			throw INTEGER_OVERFLOW;
		numerator = (int)(answ);
        answ  = pow(denominator*1.0, power) + .05;
		if (answ > INT_MAX)
			throw INTEGER_OVERFLOW;
		denominator = (int)(answ);
    }
	simplify(numerator, denominator);
	return *this;
}

ostream &operator <<(ostream &out, const Fraction &other)
{
    out << other.show();
    return out;
}

inline
void safeMultEq(int &a, const int &b)
{
	if (b && abs(a) > INT_MAX / abs(b))  //if the result of the muliplication will be larger than the max size of the interger
		throw INTEGER_OVERFLOW; //throw an error
	a *= b;  //otherwise do the operation
}
inline
int safeMult(int a, const int &b)
{
	safeMultEq(a,b);  //a is passed by value so do a saveMultEq on it
	return a;   //and return it
}

inline
void safeAddEq(int &a, const int &b)
{
	int temp;
	if (a==0)   //if a is 0 there won't be an overflow
		a = b;
	else if (a < 0)
	{
		if (b > 0)  //if the signs are opposite we're ok
			a += b;
		else if((temp = a + b) < 0)  //if the sum is the same sign we're ok
			a = temp;
		else
			throw INTEGER_OVERFLOW;  //otherwise throw the error
	}
	else  //a > 0 - do as above
	{
		if (b < 0)
			a += b;
		else if((temp = a + b) > 0)
			a = temp;
		else
			throw INTEGER_OVERFLOW;
	}
}
