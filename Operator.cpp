#include "Operator.h"

Operator::Operator (char Op, int Priority, opFPtr OpFunc)
{
	op = Op;
	priority = Priority;
	opFunc = OpFunc;
}

void Operator::operate(MixedNumber *MN1, MixedNumber *MN2 )
{
	if(!opFunc)
		throw UNKNOWN_ERROR;   //indicate programming error
	opFunc(MN1, MN2);
}

bool Operator::isLeftPar()
{
	return (op == '(');
}

bool Operator::isRightPar()
{
	return (op == ')');
}

bool Operator::twoNumsNeeded()
{
	return (priority != 4);  //the unary operator is the only one that takes only one, and it is the only with the priorty of 4.
}

std::string Operator::show()
{
	return string(1,op);
}

bool operator>(const Operator &lhs, const Operator &rhs)
{
	return (lhs.priority > rhs.priority);
}

bool operator==(const Operator &lhs, const Operator &rhs)
{
	return (lhs.priority == rhs.priority);
}

Operator* getOP(char* &cPtr)
{
	Operator* builder = NULL;
	switch (*cPtr)
	{
	case '+':
		builder = new Operator(*cPtr, 1, addOP);
		break;
	case '-':
	{
		char* checkLeft = cPtr -1;
		skipSpace(checkLeft, true);
		if(*checkLeft == '+' || *checkLeft == '-' || *checkLeft == '*' || *checkLeft == '/' || *checkLeft == '^' || *checkLeft == '(' || *checkLeft == '\0') //if to the left is another operator, not including ( or )
			builder = new Operator('~', 4, negOP);  //then this should be unary negation
		else
			builder = new Operator(*cPtr, 1, subOP);  //otherwise this should be subtraction
		break;
	}
	case '*':
		builder = new Operator(*cPtr, 2, mulOP);
		break;
	case '/':
		builder = new Operator(*cPtr, 2, divOP);
		break;
	case '^':
		builder = new Operator(*cPtr, 3, powOP);
		break;
	case ')':
	case '(':
		builder = new Operator(*cPtr, 0, NULL);
	}
	if (builder)
	{
		++cPtr;
		skipSpace(cPtr);   // in include.h
	}
	return builder;
}

void mulOP(MixedNumber* MN1, MixedNumber* MN2)
{
	*MN1 *= *MN2;
}

void divOP(MixedNumber* MN1, MixedNumber* MN2)
{
	*MN1 /= *MN2;
}

void subOP(MixedNumber* MN1, MixedNumber* MN2)
{
	*MN1 -= *MN2;
}

void addOP(MixedNumber* MN1, MixedNumber* MN2)
{
	*MN1 += *MN2;
}

void powOP(MixedNumber* MN1, MixedNumber* MN2)
{
	*MN1 ^= *MN2;
}

void negOP(MixedNumber* MN1, MixedNumber*)
{
	*MN1 *= MixedNumber(-1);
}
