#ifndef NODE_H
#define NODE_H

#include "include.h"
#include "Helpers.h"

template<typename T = char>
class node
{
    public:
        node();
        node(const T &d);
        node(const node<T> &other);
        ~node();

        node<T>& operator=(const node<T> &other);
        T& theData();
        node<T>*& nextNode();

		const T& theData()const;
        const node<T>* nextNode()const;

        template<typename U>
        friend std::ostream& operator<<(std::ostream &out, const node<U> &other);

        template<typename U>
        friend std::istream& operator>>(std::istream &in, node<U> &other);

        template<typename U>
        friend bool operator==(const node<U>& x, const node<U> &y);

        template<typename U>
        friend bool operator< (const node<U>& x, const node<U> &y);

        template<typename U>
        friend bool operator>(const node<U>& x, const node<U> &y);

        template<typename U>
        friend bool operator>=(const node<U>& x, const node<U> &y);

        template<typename U>
        friend bool operator<=(const node<U>& x, const node<U> &y);

        template<typename U>
        friend bool operator!=(const node<U>& x, const node<U> &y);

    private:
        T data;
        node<T> *next;
        void copy(const node<T> &other);
};

template<typename T>
node<T>::node()
{
    initIt(data);
	next = NULL;
}

template<typename T>
node<T>::node(const T &d)
{
    initIt(data);
    copyIt(data, d);
    next = NULL;
}

template<typename T>
node<T>::~node()
{
    delDy(data);
	next = NULL;
}

template<typename T>
node<T>::node(const node<T> &other)
{
    copy(other);
}

template<typename T>
node<T>& node<T>::operator=(const node<T> &other)
{
    if(this != &other)
        copy(other);
    return *this;
}

template<typename T>
T& node<T>::theData()
{
    return data;
}

template<typename T>
node<T>*& node<T>::nextNode()
{
    return next;
}

template<typename T>
const T& node<T>::theData() const
{
    return data;
}

template<typename T>
const node<T>* node<T>::nextNode() const
{
    return next;
}

template<typename U>
std::ostream& operator<<(std::ostream &out, const node<U> &other)
{
    out<<other.data << " ";
    return out;
}

template<typename U>
std::istream& operator>>(std::istream &in, node<U> &other)
{
    if(!in>>other.data)
    {
		throw BAD_INPUT;
    }
    return in;
}

template<typename U>
bool operator==(const node<U>& x, const node<U> &y)
{
    return isEq(x.data == y.data);
}

template<typename U>
bool operator>(const node<U>& x, const node<U> &y)
{
	return isGr(x.data > y.data);
}

template<typename U>
bool operator<(const node<U>& x, const node<U> &y)
{
    return (y > x);
}

template<typename U>
bool operator>=(const node<U>& x, const node<U> &y)
{
    return !(y > x);
}

template<typename U>
bool operator<=(const node<U>& x, const node<U> &y)
{
    return !(x > y);
}

template<typename U>
bool operator!=(const node<U>& x, const node<U> &y)
{
    return !(x == y);
}

/////////////private function//////////////
template<typename T>
inline
void node<T>::copy(const node<T> &other)
{
    initIt(data);
    copyIt(data,other.data);
    next = NULL;
}

#endif // NODE_H
